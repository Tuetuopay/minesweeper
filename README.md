# Minesweeper #

![Grind](http://up.tuetuopay.fr/GrindGearsMinesweeper.jpg)

What a shame ... Windows always had an embedded Minesweeper, and OS X never got a proper one. At the time of writing this one, the Mac App Store had only poorly programmed ones. Here's another one :D

![Screenshot](http://up.tuetuopay.fr/Mine_1.png)

***

## Getting and compiling ##

The source code is 100% cross-platform, though the project file shipped is only for Xcode >= 4 on OS X. You will need SDL 1.2 installed in both your `Xcode.app` library and in the system library folder (`/System/Library/Frameworks`).

1. Clone
2. Open `Minesweeper.xcodeproj`
3. Hit `Build and Run` button
4. Voilà !

***

## Special feature ##

There is a kind of LSD mode ... Since this is made with OpenGL, it is ridiculously easy to apply rotations to tiles ! When playing, hit the `L` key on your keyboard.

Here is the previous screenshot with LSD mode enabled :
![Screenshot](http://up.tuetuopay.fr/Mine_2.png)

***

## Additionnal notes ##

Once the game was far more advanced : I had score management and saving, board size, mine count, settings window, etc... But I lost it all thanks to the HDD crash my MacBook Pro had in the late 2013.