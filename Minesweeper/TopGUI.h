//
//  TopGUI.h
//  Minesweeper
//
//  Created by Alexis on 07/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _TOP_GUI_H
#define _TOP_GUI_H

#include <iostream>
#include "RenderEngine.h"

extern class RenderEngine;
extern class RenderArgs;

class TopGUI	{
public:
	TopGUI (double width, double heigth, double pixelPerUnit);
	~TopGUI ();
	
	/* Redefine the GUI */
	void resizeGUI (double width, double heigth, double pixelPerUnit);
	
	/* Coordinates of the mouse in PIXELS
	 * returns if the button has been clicked
	 ***/
	bool updateEvents (bool clicking, int mouseX, int mouseY);
	
	/* Rendering */
	void render (RenderArgs &renderArgs, RenderEngine &renderEngine);
	
	
private:
	/* GUI Width and height in GL units */
	double _width, _height, _pixelPerUnit;
	
	/* Wether or not we should render the "new game" button
	 * as toggled or not
	 ***/
	bool _isToggled;
};

#endif /* defined(_TOP_GUI_H) */
