//
//  RenderEngine.h
//  Minesweeper
//
//  Created by Alexis on 13/04/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _RENDER_ENGINE_H
#define _RENDER_ENGINE_H

#include <iostream>
#include <SDL/SDL.h>
#include <SDL_image/SDL_image.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

#include "sdlglutils.h"
#include "Game.h"
#include "TextEngine.h"
#include "TopGUI.h"
#include "GUIScreen.h"
#include "GUITextEdit.h"

#define TILE_SIZE	25		// Tile size in px, the .png file is 64x64

extern class TopGUI;

typedef class RenderEngine;

class RenderArgs
{
public:
	RenderArgs (TopGUI &_topGUI, GUIScreen &testScreen) : topGUI(_topGUI), _testScreen (testScreen)	{}
	
	int nTilesWidth;
	int nTilesHeight;
	
	int nMines;
	int nFlags;
	
	int **gameStatus;
	
	TopGUI	&topGUI;
	
	bool chronoRunning;
	int chronoTime;
	bool lsdMode;
	
	RenderEngine *renderEngine;
	
	/* Testing my GUI lib */
	GUIScreen &_testScreen;
};

class RenderEngine	{
public:
	RenderEngine ( RenderArgs &renderArgs, bool setup = true );
	~RenderEngine ();
	
	/* Well ... I think the name is pretty self explicit */
	void render ( RenderArgs &renderArgs );
	
	/* Will reset up all the graphics settings (aka. window size) */
	void reload ( RenderArgs &renderArgs );
	
	/* Loading all resources (textures) & building Display Lists */
	void loadResources ();
	
	/* Rendering the button */
	void renderButton ()	{ glCallList (_dlButton); }
	
private:
	/* Will initialize SDL and create an OpenGL context with SDL */
	void setupGraphics ();
	
	/* Rendering methods, called by .render () */
	void drawGL ( RenderArgs &renderArgs );		/* Won't be using this, only in 2D */
	void drawHUD ( RenderArgs &renderArgs );
	
	/* Will set up everything for the render context (SDL & OpenGL) */
	bool load ( RenderArgs &renderArgs );
	
	/* These are the 4 textures used in the game: the basic tile, the flag, the bomb and the new game button */
	GLuint	_texTile;
	GLuint	_texFlag;
	GLuint	_texBomb;
	GLuint	_texButton;
	
	/* Display lists used for speed */
	GLuint _dlTile, _dlFlag, _dlBomb, _dlButton;
	
	/* This will render the numbers */
	TextEngine	_textEngine;
	
	/* Builders & loaders */
	void loadTextures ();
	void buildDisplayLists ();
	
	bool _wasLoaded;
};

#endif /* defined(_RENDER_ENGINE_H) */












