//
//  GUITextEdit.h
//  Minesweeper
//
//  Created by Alexis on 23/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_TEXT_EDIT
#define _GUI_TEXT_EDIT

#include <iostream>
#include <cmath>

#include "GUIElement.h"

class GUITextEdit : public GUIElement
{
public:
	GUITextEdit (double width, double height, double x, double y, std::string defaultText = "");
	~GUITextEdit ();
	
	void render ();
	void processLeftClick (double x, double y, bool buttonUp);
	void processKeypress (SDL_KeyboardEvent keyboardEvent);
	
	const std::string& text ();
	void setText (const std::string& text);
	
private:
	std::string _text, _maxText;
	
	bool _clickBeganOnMe;
	bool _editing;
};

#endif /* defined(_GUI_TEXT_EDIT) */
