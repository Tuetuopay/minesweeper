//
//  TextEngine.cpp
//  Minesweeper
//
//  Created by Alexis on 25/04/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "TextEngine.h"

/* Declaring the ststic members */
bool	TextEngine::_resourcesLoaded = false;
GLuint	TextEngine::_textures[10] = {0};
GLuint	TextEngine::_texMinus = 0;
GLuint	TextEngine::_texDefault = 0;
GLuint	TextEngine::_dlNumbers[10] = {0};
GLuint	TextEngine::_dlMinus = 0;
GLuint	TextEngine::_dlDefault[256] = {0};

TextEngine::TextEngine ()
{
	TextEngine::_resourcesLoaded = false;
}

TextEngine::~TextEngine()
{
	
}

void TextEngine::drawString (const char text[], float r, float g, float b)
{
	glPushMatrix();
	glColor3f (r, g, b);
	
	for (int i = 0; i < strlen(text); i++)	{
		glCallList (_dlDefault[text[i] + ((text[i] > 0) ? 0 : 256)] );
		glTranslatef (0.6, 0.0, 0.0);
	}
	glPopMatrix();
}

void TextEngine::drawColoredNumber (int number)
{
	if (number >= 10)	return;
	switch (number)	{
		case 1:
			glColor3ub (4, 52, 255);
			break;
		case 2:
			glColor3ub (0, 125, 0);
			break;
		case 3:
			glColor3ub (255, 38, 0);
			break;
		case 4:
			glColor3ub (1, 20, 128);
			break;
		case 5:
			glColor3ub (130, 13, 0);
			break;
		case 6:
			glColor3ub (0, 127, 128);
			break;
		case 7:
			glColor3ub (2, 2, 2);
			break;
		case 8:
			glColor3f (0.5, 0.5, 1.0);
			break;
	}
	glCallList (_dlNumbers[number]);
}

void TextEngine::drawNumber (int number, float r, float g, float b)
{
	char nbr[20] = "";	/* A 32-bit integer is roughly < 16 chars */
	int i = 0;
	
	sprintf (nbr, "%d", number);
	
	glColor3f (r, g, b);
	glPushMatrix();
	if (nbr[0] == '-')	{
		glCallList (_dlMinus);
		glTranslated (0.6, 0.0, 0.0);
		i++;
	}
	for (; i < strlen (nbr); i++)	{
		glCallList (_dlNumbers[nbr[i] - '0']);
		glTranslated (0.6, 0.0, 0.0);
	}
	glPopMatrix();
}

void TextEngine::drawBackground (int number, float rBG, float gBG, float bBG, float aBG)
{
	drawBackground(number, rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}

void TextEngine::drawBackground (int number,
									  float rBG, float gBG, float bBG, float aBG,
									  float rOL, float gOL, float bOL, float aOL)
{
	char nbr[20] = "";
	sprintf (nbr, "%d", number);
	drawBackground(nbr, rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}

void TextEngine::drawBackground (char str[], float rBG, float gBG, float bBG, float aBG)
{
	drawBackground(str, rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}

void TextEngine::drawBackground (char str[],
					 float rBG, float gBG, float bBG, float aBG,
					 float rOL, float gOL, float bOL, float aOL)
{
	/* First, compute the width, one number is .6 wide, including margins between number
	 * then adding the left margin
	 */
	double width = getStringWidth (str) * 0.6;
	
	glPushMatrix();
	glTranslated (0.1, 0.05, 0.0);
	glLineWidth (1.0);
	
	glDisable (GL_TEXTURE_2D);
	
	glColor4f (rBG, gBG, bBG, aBG);
	glBegin (GL_QUADS);
		glVertex2d (0.0, 0.0);
		glVertex2d (width, 0.0);
		glVertex2d (width, 1.0);
		glVertex2d (0.0, 1.0);
	glEnd ();
	
	glColor4f (rOL, gOL, bOL, aOL);
	glBegin (GL_LINE_STRIP);
		glVertex2d (0.0, 0.0);
		glVertex2d (width, 0.0);
		glVertex2d (width, 1.0);
		glVertex2d (0.0, 1.0);
		glVertex2d (0.0, 0.0);
	glEnd ();
	glPopMatrix();
}

void TextEngine::drawBackground (std::string& str, float rBG, float gBG, float bBG, float aBG)
{
	drawBackground((char*)str.c_str(), rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}

void TextEngine::drawBackground (std::string& str,
					 float rBG, float gBG, float bBG, float aBG,
					 float rOL, float gOL, float bOL, float aOL)
{
	drawBackground((char*)str.c_str(), rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}

float TextEngine::getStringWidth (const std::string& string)
{
	return getStringWidth (string.c_str());
}

float TextEngine::getStringWidth (const char string[])
{
	return (float)strlen (string) + 0.2;
}

void TextEngine::loadTextures()
{
	if (_resourcesLoaded)	return;
	_resourcesLoaded = true;
	
	char filename[100] = "";
	
	for (int i = 0; i < 10; i++)	{
		sprintf (filename, "font/%d.png", i);
		_textures[i] = loadTexture (filename);
	}
	
	_texMinus = loadTexture ("font/misc/-.png");
	_texDefault = loadTexture ("font/default.png");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

	buildDisplayLists();
}

void TextEngine::buildDisplayLists()
{
	for (int i = 0; i < 10; i++)	{
		_dlNumbers[i] = glGenLists (1);
		
		glNewList (_dlNumbers[i], GL_COMPILE);
			glPushMatrix();
			glEnable (GL_TEXTURE_2D);
			glBindTexture (GL_TEXTURE_2D, _textures[i]);
			glScaled (0.8, 0.8, 1.0);
			glTranslated (0.1, 0.2, 0);
			glBegin (GL_QUADS);
				glTexCoord2d(1.0, 0.0);	glVertex2d(1.0, 1.0);
				glTexCoord2d(1.0, 1.0);	glVertex2d(1.0, 0.0);
				glTexCoord2d(0.0, 1.0);	glVertex2d(0.0, 0.0);
				glTexCoord2d(0.0, 0.0);	glVertex2d(0.0, 1.0);
			glEnd ();
			glPopMatrix();
		glEndList();
	}
	
	for (int v = 0; v < 16; v++)	{
		for (int u = 0;u < 16; u++)	{
			_dlDefault[(15 - v) * 16 + u] = glGenLists(1);
			glNewList (_dlDefault[(15 - v) * 16 + u], GL_COMPILE);
				glPushMatrix();
				glEnable (GL_TEXTURE_2D);
				glBindTexture (GL_TEXTURE_2D, _texDefault);
				glScaled (0.8, 0.8, 1.0);
				glTranslated (0.1, 0.2, 0);
				glBegin (GL_QUADS);
					glTexCoord2d(((double)u + 1.0) / 16.0, (double)v / 16.0);			glVertex2d(1.0, 1.0);
					glTexCoord2d(((double)u + 1.0) / 16.0, ((double)v + 1.0) / 16.0);	glVertex2d(1.0, 0.0);
					glTexCoord2d((double)u / 16.0, ((double)v + 1.0) / 16.0);			glVertex2d(0.0, 0.0);
					glTexCoord2d((double)u / 16.0, (double)v / 16.0);					glVertex2d(0.0, 1.0);
				glEnd ();
				glPopMatrix();
			glEndList();
		}
	}
	
	_dlMinus = glGenLists (1);
	
	glNewList (_dlMinus, GL_COMPILE);
		glPushMatrix();
		glEnable (GL_TEXTURE_2D);
		glBindTexture (GL_TEXTURE_2D, _texMinus);
		glScaled (0.8, 0.8, 1.0);
		glTranslated (0.1, 0.2, 0);
		glBegin (GL_QUADS);
			glTexCoord2d(1.0, 0.0);	glVertex2d(1.0, 1.0);
			glTexCoord2d(1.0, 1.0);	glVertex2d(1.0, 0.0);
			glTexCoord2d(0.0, 1.0);	glVertex2d(0.0, 0.0);
			glTexCoord2d(0.0, 0.0);	glVertex2d(0.0, 1.0);
		glEnd ();
		glPopMatrix();
	glEndList();
}
