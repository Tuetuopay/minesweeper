//
//  TopGUI.cpp
//  Minesweeper
//
//  Created by Alexis on 07/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "TopGUI.h"

TopGUI::TopGUI (double width, double heigth, double pixelPerUnit)
	: _width(width), _height(heigth), _pixelPerUnit(pixelPerUnit)
{
	_isToggled = false;
}

TopGUI::~TopGUI ()
{
	
}

void TopGUI::resizeGUI (double width, double heigth, double pixelPerUnit)
{
	_width = width;	_height = heigth;	_pixelPerUnit = pixelPerUnit;
}

void TopGUI::render (RenderArgs &renderArgs, RenderEngine &renderEngine)
{
	glEnable (GL_TEXTURE_2D);
	
	/* Rendering the new game button */
	glPushMatrix();
	glTranslated (_width / 2.0, _height / 2.0, 0);
	if (_isToggled)	glColor3f (0.5, 0.5, 0.5);
	else			glColor3f (1.0, 1.0, 1.0);
	renderEngine.renderButton();
	glPopMatrix();
	
	glDisable (GL_TEXTURE_2D);
}

bool TopGUI::updateEvents (bool clicking, int mouseX, int mouseY)
{
	double x = (double)mouseX / _pixelPerUnit,
	       y = (double)mouseY / _pixelPerUnit;
	bool toReturn = false;
	
	if (x >= (_width / 2.0) - 0.5 && x <= (_width / 2.0) + 0.5 &&
		y >= (_height / 2.0) - 0.5 && y <= (_height / 2.0) + 0.5)	{	/* If we are in the button */
		if (_isToggled && !clicking)	toReturn = true;
		else							toReturn = false;
		_isToggled = clicking;
	} else	_isToggled = false;
	
	return toReturn;
}


