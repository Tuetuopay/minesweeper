//
//  GUIScreen.h
//  Minesweeper
//
//  Created by Alexis on 16/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_SCREEN_H
#define _GUI_SCREEN_H

#include <iostream>
#include <vector>

#include "GUI.h"
#include "GUIElement.h"

class GUIScreen : public GUI
{
public:
	/* Same as GUI's constructor, look in GUI.h */
	GUIScreen (double width, double height, double x = 0.0, double y = 0.0);
	~GUIScreen ();
	
	/* Renders the current GUI screen */
	void render ();
	
	/* Event managing */
	void processLeftClick (double x, double y, bool buttonUp);	/* x & y are given in GL coordinates */
	void processRightClick (double x, double y, bool buttonUp);
	void mouseMotion (double x, double y, bool isClicking);
	void processKeypress (SDL_KeyboardEvent keyboardEvent);
	
	/* GUI visibility */
	void show ()	{ _isShown = true; }
	void hide ()	{ _isShown = false; }
	void setShown (bool shown)	{ _isShown = shown; }
	bool isShown ()	{ return _isShown; }
	
public:
	/* Will push this element onto te GUI screen */
	void addElement (GUIElement *guiElement);
	
private:
	/* Stores everything inside this GUI screen */
	std::vector<GUIElement*>	_guiElements;
	int _nElements;
	
	/* If this is the current GUI shown */
	bool _isShown;
	
	/* Background of the GUI */
	GLcolor _bgColorTopLeft, _bgColorTopRight, _bgColorBottomLeft, _bgColorBottomRight;
	GLcolor _olColorTopLeft, _olColorTopRight, _olColorBottomLeft, _olColorBottomRight;
	GLuint  _bgTexture;
};

#endif /* defined(_GUI_SCREEN_H) */
