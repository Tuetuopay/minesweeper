//
//  Game.h
//  Minesweeper
//
//  Created by Alexis on 23/04/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GAME_H
#define _GAME_H


#define TILE_UNTOUCHED	0x00	/* 00000000 */
#define TILE_CLEARED	0x10	/* 00010000 */
#define TILE_BOMB		0x20	/* 00100000 */
#define TILE_FLAGGED	0x40	/* 01000000 */
#define TILE_CLICKED	0x80	/* 10000000 */

#define TILE_VALUE_MASK	0x0F	/* 00001111 */

#endif
