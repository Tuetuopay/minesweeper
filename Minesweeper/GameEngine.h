//
//  GameEngine.h
//  Minesweeper
//
//  Created by Alexis on 19/04/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GAME_ENGINE_H
#define _GAME_ENGINE_H

#include <iostream>
#include <string>

#include "RenderEngine.h"
#include "Game.h"
#include "TopGUI.h"
#include "GUIScreen.h"

#define FPS 24

void printErrorAndExit (std::string error);
int aleatoire ( int min, int max );

class GameEngine
{
public:
	GameEngine ();
	~GameEngine ();
	
	void run ();
	
private:
	/* ===== VAR ===== */
	/* The render engine, along with it's args */
	RenderEngine	_renderEngine;
	RenderArgs		_renderArgs;
	
	/* The GUI at the top of the screen */
	TopGUI			_topGUI;
	
	/* The ID of the timer responsible of the chrono */
	SDL_TimerID		_chronoTimerID;
	
	/* Is the game running ? */
	bool _keepRunning;
	
	/* Are we clicking ? */
	bool _isClicking;
	
	/* Did we lost the game ? (used to stop all interactions when lost) */
	bool _gameLost;
	
	/* Is the shift button pressed ? */
	bool _isShiftToggled;
	
	/* ===== METHODS ===== */
	/* Main game loop, managing FPS and events */
	void mainLoop ();
	
	/* Basically manages all events, called once a frame */
	void processEvents ();
	/* Processes all the actions when left-clicking and right clicking.
	 * buttonUp is set to true when the event is SDL_MOUSEBUTTONUP, false when SDL_MOUSEBUTTONDOWN
	 */
	void processLeftClick (int x, int y, bool buttonUp);
	void processRightClick (int x, int y, bool buttonUp);
	/* Processes when moving the mouse */
	void processMouseMotion (int x, int y);
	
	/* Resets & generates a new board */
	void computeBoard ();
	
	/* Will clear a whole blank area when a blank tile is cleared */
	bool spreadZeros ();
	
	/* Will just set mine tiles as cleared
	 * used to show mines at the end of the game
	 ***/
	void revealMines ();
	
	GUIScreen _testGUI;
};

Uint32 chronoTimer (Uint32 callbackDelay, void *args);

#endif /* defined(_GAME_ENGINE_H) */




