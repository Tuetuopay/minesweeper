//
//  GameEngine.cpp
//  Minesweeper
//
//  Created by Alexis on 19/04/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GameEngine.h"

GameEngine::GameEngine () : _topGUI(_renderArgs.nTilesWidth, 2.0, TILE_SIZE), _testGUI(10.0, 10.0, 3.0, 3.0), _renderArgs(_topGUI, _testGUI), _renderEngine(_renderArgs, false),
							_keepRunning(true), _isClicking(false), _gameLost(false), _isShiftToggled(false)
{
	_renderArgs.nTilesHeight = 16;
	_renderArgs.nTilesWidth = 30;
	_renderArgs.nMines = 89;
	_renderArgs.nFlags = 0;
	_renderArgs.chronoTime = 0;
	_renderArgs.chronoRunning = false;
	_renderArgs.lsdMode = false;
	
	_renderArgs.renderEngine = &_renderEngine;
	
	if (!(_renderArgs.gameStatus = new int*[_renderArgs.nTilesWidth]))	printErrorAndExit ("Failed to allocate some memory");
	for (int i = 0; i < _renderArgs.nTilesWidth; i++)
		if (!(_renderArgs.gameStatus[i] = new int[_renderArgs.nTilesHeight]))	printErrorAndExit ("Failed to allocate some more memory");
	
	computeBoard();
	
	_renderEngine.reload (_renderArgs);
	_renderEngine.loadResources();
	_topGUI.resizeGUI(_renderArgs.nTilesWidth, 2.0, TILE_SIZE);
	_renderArgs.topGUI = _topGUI;
	
	_renderArgs._testScreen.addElement (new GUITextEdit (20.0, 1.0, 3.2, 1.2, "Tést text !"));
	
	SDL_AddTimer (100, chronoTimer, &_renderArgs);
}

GameEngine::~GameEngine()
{
	for (int i = 0; i < _renderArgs.nTilesWidth; i++)
		delete _renderArgs.gameStatus[i];
	delete _renderArgs.gameStatus;
}

void GameEngine::run ()
{
	mainLoop ();
}

void GameEngine::mainLoop ()
{	
	while (_keepRunning)	{
		processEvents ();
		
		_renderEngine.render (_renderArgs);
		
		SDL_Delay (1000/FPS);
	}
}

void GameEngine::processEvents ()
{
	static SDL_Event ev;
	SDL_EnableKeyRepeat (10, 10);
	
	while (SDL_PollEvent (&ev))	{
	/* SDL_WaitEvent(&ev); */
		switch (ev.type)	{
			case SDL_QUIT:
				_keepRunning = false;
				break;
				
			case SDL_KEYDOWN:
				switch (ev.key.keysym.sym)	{
					case SDLK_ESCAPE:
						_keepRunning = false;
						break;
						
					case SDLK_F2:
					case SDLK_n:
						computeBoard();
						break;
						
					case SDLK_LSHIFT:
					case SDLK_RSHIFT:
						_isShiftToggled = true;
						break;
						
					case SDLK_g:
						_renderArgs._testScreen.show ();
						break;
						
					case SDLK_h:
						_renderArgs._testScreen.hide ();
						break;
						
					case SDLK_l:
						_renderArgs.lsdMode = !_renderArgs.lsdMode;
						break;
						
					default:
						break;
				}
				_renderArgs._testScreen.processKeypress (ev.key);
				break;
			
			case SDL_KEYUP:
				switch (ev.key.keysym.sym)	{
					case SDLK_LSHIFT:
					case SDLK_RSHIFT:
						_isShiftToggled = false;
						break;
						
					default:
						break;
				}
				break;
				
			case SDL_MOUSEBUTTONDOWN:
				processLeftClick (ev.button.x, ev.button.y, false);
				break;
				
			case SDL_MOUSEBUTTONUP:
				switch (ev.button.button)	{
					case SDL_BUTTON_LEFT:
						processLeftClick (ev.button.x, ev.button.y, true);
						break;
						
					case SDL_BUTTON_RIGHT:
						processRightClick (ev.button.x, ev.button.y, true);
						break;
						
					default:
						break;
				}
				_isClicking = false;
				
			case SDL_MOUSEMOTION:
				processMouseMotion (ev.button.x, ev.button.y);
				break;
				
			default:
				break;
		}
	}
}

void GameEngine::processLeftClick (int x, int y, bool buttonUp)
{
	/* Updating the GUI's */
	_renderArgs._testScreen.processLeftClick ((double)x / (double)TILE_SIZE, (double)y / (double)TILE_SIZE - 2.0, buttonUp);
	
	if (!buttonUp)	{
		if (y >= TILE_SIZE * 2 && !_gameLost)	/* if we're in the board (not in the top GUI) and we didn't lost */
		{
			y -= TILE_SIZE * 2;
			_renderArgs.gameStatus[x / TILE_SIZE][y / TILE_SIZE] |= TILE_CLICKED;
		} else
			_topGUI.updateEvents (true, x, y);
		
		_isClicking = !_gameLost;
	}
	else
	{
		/* If we're releasing the button */
		if (y >= TILE_SIZE * 2 && !_gameLost) /* if we're in the board (not in the top GUI) and we didn't lost */
		{
			y -= TILE_SIZE * 2; /* Translating to board local coordinates */
			x /= TILE_SIZE;	y /= TILE_SIZE;		/* Converting mouse xy to board xy */
			
			if (!(_renderArgs.gameStatus[x][y] & TILE_FLAGGED))
				_renderArgs.gameStatus[x][y] |= TILE_CLEARED;
			_renderArgs.gameStatus[x][y] &= ~TILE_CLICKED;
			
			/* If we clicked on a blank area */
			if ((_renderArgs.gameStatus[x][y] & TILE_VALUE_MASK) == 0)
				while (spreadZeros());
			
			/* If we clicked on a mine */
			if (_renderArgs.gameStatus[x][y] & TILE_BOMB &&
				_renderArgs.gameStatus[x][y] & TILE_CLEARED)	{
				revealMines ();
				_gameLost = true;
			}
			
			/* If we clicked on an already cleared tile while holding shift WITHOUT a flag */
			if (_renderArgs.gameStatus[x][y] & TILE_CLEARED && !(_renderArgs.gameStatus[x][y] & TILE_FLAGGED) && _isShiftToggled)	{
				/* Then we check if there are as many surrounding flags as bombs surrounding */
				int nFlags = 0;
				for (int i = x - 1; i <= x+1; i++)	{
					for (int j = y - 1; j <= y + 1; j++)	{
						if (!(x == i && y == j) &&
							i >= 0 && i < _renderArgs.nTilesWidth &&
							j >= 0 && j < _renderArgs.nTilesHeight)	{
							if (_renderArgs.gameStatus[i][j] & TILE_FLAGGED) nFlags++;
						}
					}
				}
				
				/* If we have as many flags as surrounding bombs */
				if (nFlags == (_renderArgs.gameStatus[x][y] & TILE_VALUE_MASK))	{
					_isShiftToggled = false;	/* Prevents infinite callbacks to processLeftClick */
					for (int i = x - 1; i <= x+1; i++)	{
						for (int j = y - 1; j <= y + 1; j++)	{
							if (i >= 0 && i < _renderArgs.nTilesWidth && j >= 0 && j < _renderArgs.nTilesHeight)
								/* Basically we're clearing the tile, and using processLeftClick allows us to clear AND
								   check for loss */
								processLeftClick ((i * TILE_SIZE), (j + 2) * TILE_SIZE, true);
						}
					}
					_isShiftToggled = true;
				}
			}
			
			_renderArgs.chronoRunning = !_gameLost;
		}
		else if (_topGUI.updateEvents (false, x, y))
			computeBoard();
	}
}

void GameEngine::processRightClick (int x, int y, bool buttonUp)
{
	if (!buttonUp)	processLeftClick (x, y, buttonUp);	/* Currently the code is the same when SDL_MOUSEBUTTONDOWN */
	else	{
		if (y >= TILE_SIZE * 2 && !_gameLost)	{
			y -= TILE_SIZE * 2;
			/* If the tile weren't cleared */
			if (!(_renderArgs.gameStatus[x / TILE_SIZE][y / TILE_SIZE] & TILE_CLEARED))	{
				/* Then we invert the flag bit */
				_renderArgs.gameStatus[x / TILE_SIZE][y / TILE_SIZE] ^= TILE_FLAGGED;
				/* And finally change the amount of lags curently in count */
				if (_renderArgs.gameStatus[x / TILE_SIZE][y / TILE_SIZE] & TILE_FLAGGED)
					_renderArgs.nFlags++;
				else _renderArgs.nFlags--;
			}
			_renderArgs.gameStatus[x / TILE_SIZE][y / TILE_SIZE] &= ~TILE_CLICKED;
		}
	}
}

void GameEngine::processMouseMotion (int x, int y)
{
	/* GUI event management */
	_renderArgs._testScreen.mouseMotion ((double)x / (double)TILE_SIZE, (double)y / (double)TILE_SIZE - 2.0, _isClicking);
	
	if (_isClicking)	{
		if (y >= TILE_SIZE * 2)	{
			y -= TILE_SIZE * 2;
			for (int i = 0; i < _renderArgs.nTilesWidth; i++)	{
				for (int j = 0; j < _renderArgs.nTilesHeight; j++)
					_renderArgs.gameStatus[i][j] &= ~TILE_CLICKED;
			}
			_renderArgs.gameStatus[x / TILE_SIZE][y / TILE_SIZE] |= TILE_CLICKED;
		} else if (_topGUI.updateEvents (true, x, y))
			computeBoard();
	}
}

bool GameEngine::spreadZeros()
{
	int zerosSpreaded = 0;
	/* Running through the board */
	for (int i = 0; i < _renderArgs.nTilesWidth; i++)	{
		for (int j = 0; j < _renderArgs.nTilesHeight; j++)	{
			/* If the tile value is Zero AND if it has already been cleared AND if it is NOT a bomb */
			if (((_renderArgs.gameStatus[i][j] & TILE_VALUE_MASK) == 0) &&
				(_renderArgs.gameStatus[i][j] & TILE_CLEARED) &&
				!(_renderArgs.gameStatus[i][j] & TILE_BOMB))	{
				/* Then we run through the 8 surrounding tile */
				for (int x = i - 1; x <= i + 1; x++)	{
					for (int y = j - 1; y <= j + 1; y++)	{
						/* If the [x][y] tile is NOT a bomb AND has NOT been cleared yet */
						if (x >= 0 && y >= 0 && x < _renderArgs.nTilesWidth && y < _renderArgs.nTilesHeight)	{
							if (!(_renderArgs.gameStatus[x][y] & TILE_BOMB) &&
								!(_renderArgs.gameStatus[x][y] & TILE_CLEARED) &&
								!(_renderArgs.gameStatus[x][y] & TILE_FLAGGED))	{
								_renderArgs.gameStatus[x][y] |= TILE_CLEARED;
								zerosSpreaded++;
							}
						}
					}
				}
			}
		}
	}
	
	if (zerosSpreaded)	return true;
	else				return false;
}

void GameEngine::computeBoard()
{
	/* Clearing board */
	for (int i = 0; i < _renderArgs.nTilesWidth; i++)	{
		for (int j = 0; j < _renderArgs.nTilesHeight; j++)	{
			_renderArgs.gameStatus[i][j] = TILE_UNTOUCHED;
		}
	}
	_renderArgs.nFlags = 0;
	_renderArgs.chronoRunning = false;
	_renderArgs.chronoTime = 0;
	_gameLost = false;
	
	/* Generating bombs */
	for (int i = 0; i < _renderArgs.nMines; i++)	{
		int x = aleatoire(0, _renderArgs.nTilesWidth - 1), y = aleatoire(0, _renderArgs.nTilesHeight - 1);
		if (_renderArgs.gameStatus[x][y] & TILE_BOMB)
			i--;
		else
			_renderArgs.gameStatus[x][y] |= TILE_BOMB;
	}
	
	/* Computing numbers */
	char value = 0;
	for (int i = 0; i < _renderArgs.nTilesWidth; i++)	{
		for (int j = 0; j < _renderArgs.nTilesHeight; j++)	{
			if (!(_renderArgs.gameStatus[i][j] & TILE_BOMB))	{
				for (int x = i - 1; x <= i + 1; x++)	{
					for (int y = j - 1; y <= j + 1; y++)	{
						if (x >= 0 && y >= 0 && x < _renderArgs.nTilesWidth && y < _renderArgs.nTilesHeight)	{
							if (_renderArgs.gameStatus[x][y] & TILE_BOMB)
								value++;
						}
					}
				}
				_renderArgs.gameStatus[i][j] |= value;
				value = 0;
			}
		}
	}
}

void GameEngine::revealMines ()
{
	for (int x = 0; x < _renderArgs.nTilesWidth; x++)	{
		for (int y = 0; y < _renderArgs.nTilesHeight; y++)	{
			if (_renderArgs.gameStatus[x][y] & TILE_BOMB)
				_renderArgs.gameStatus[x][y] |= TILE_CLEARED;
		}
	}
}

Uint32 chronoTimer (Uint32 callbackDelay, void *args)
{
	RenderArgs *renderArgs = (RenderArgs*)args;
	
	/* Chrono will run at 0.1s */
	if (renderArgs->chronoRunning)	renderArgs->chronoTime += callbackDelay / 100;
	
	return callbackDelay;
}

void printErrorAndExit (std::string error)
{
	std::cout << error << std::endl;
	exit (EXIT_FAILURE);
}

int aleatoire ( int min, int max )
{
	return (rand() % (max - min + 1)) + min;
}








