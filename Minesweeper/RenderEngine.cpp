//
//  RenderEngine.cpp
//  Minesweeper
//
//  Created by Alexis on 13/04/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//


#define WIN_W	640
#define WIN_H	480

#include "RenderEngine.h"

RenderEngine::RenderEngine ( RenderArgs &renderArgs, bool setup )
{
	_wasLoaded = false;
	
	_dlBomb = _dlFlag = _dlTile = 0;
	
	if (setup)
		load (renderArgs);
}

RenderEngine::~RenderEngine()
{
	SDL_Quit ();
}

void RenderEngine::drawHUD ( RenderArgs &renderArgs )
{
	/* Rendering the HUD on the top of the screen */
	/* Gradient background */
	glDisable (GL_TEXTURE_2D);
	glBegin (GL_QUADS);
		glColor3f (0.95, 0.95, 0.94);
		glVertex2d (0.0, 0.0);
		glVertex2d (renderArgs.nTilesWidth, 0.0);
		glColor3f (0.71, 0.71, 0.71);
		glVertex2d (renderArgs.nTilesWidth, 2.0);
		glVertex2d (0.0, 2.0);
	glEnd ();
	
	/* New game button */
	renderArgs.topGUI.render (renderArgs, *this);
	
	/* Test string */
	glPushMatrix();
	glTranslatef (3.0, 0.5, 0.0);
	// _textEngine.drawString ("Ceci est un tr\x8As long bouton de test !");
	glPopMatrix();
	
	/* Mines left */
	glPushMatrix();
	glScaled (1.5, 1.5, 1.0);
	glTranslated (0.15, 0.1, 0.0);
	_textEngine.drawBackground (renderArgs.nMines - renderArgs.nFlags, 0.9, 0.9, 0.9, 0.8, 0.7, 0.7, 0.7, 0.8);
	_textEngine.drawNumber (renderArgs.nMines - renderArgs.nFlags, 1.0, 0.0, 0.0);
	glPopMatrix();
	
	/* Chrono */
	glPushMatrix();
	glTranslated (renderArgs.nTilesWidth - 5, 0.0, 0.0);
	glScaled (1.5, 1.5, 1.0);
	glTranslated (0.0, 0.1, 0.0);
	_textEngine.drawBackground (renderArgs.chronoTime / 10, 0.9, 0.9, 0.9, 0.8, 0.7, 0.7, 0.7, 0.8);
	_textEngine.drawNumber (renderArgs.chronoTime / 10, 1.0, 0.0, 0.0);
	glPopMatrix();
	
	glColor3f (0.6, 0.6, 0.6);
	glTranslated (0.0, 2.0, 0.0);	/* We're not anymore in the GUI on top of the screen */
	
	/* Rendering the grid behind the board */
	glDisable (GL_TEXTURE_2D);
	glLineWidth (2.0);
	glBegin (GL_LINES);
	for (int x = 0; x <= renderArgs.nTilesWidth; x++)	{
		glVertex2d ((double)x, 0.0);
		glVertex2d ((double)x, (double)renderArgs.nTilesHeight);
	}
	for (int y = 0; y <= renderArgs.nTilesHeight; y++)	{
		glVertex2d (0.0, (double)y);
		glVertex2d ((double)renderArgs.nTilesWidth, (double)y);
	}
	glEnd ();
	
	/* Rendering the actual board */
	glEnable (GL_TEXTURE_2D);
	
	glColor3f (1.0, 1.0, 1.0);
	
	double time = ((double)SDL_GetTicks()) / 1000.0;
	for (int i = 0; i < renderArgs.nTilesWidth; i++)	{
		for (int j = 0; j < renderArgs.nTilesHeight; j++)	{
			glPushMatrix ();
			glTranslated (i, j, 0.0);
			if (renderArgs.lsdMode)	{
				glTranslated(sin(time + j) / 3.0, sin(time + i) / 3.0, 0);
				glRotated(cos(time + j + i) * 25.0, 0, 0, 1);
			}
			
			if (renderArgs.gameStatus[i][j] & TILE_CLICKED)	/* If we are currently clicking on the tile */
				glColor3f (0.7, 0.7, 0.7);
			if (!(renderArgs.gameStatus[i][j] & TILE_CLEARED))	/* If the tile has not been cleared (left clicked) */
				glCallList (_dlTile);
			else if ((renderArgs.gameStatus[i][j] & TILE_VALUE_MASK) != 0)	{	/* If the tile was cleared and contains a number */
				glColor3f (1.0, 1.0, 1.0);										/* different from 0 (aka blank areas) */
				_textEngine.drawColoredNumber (renderArgs.gameStatus[i][j] & TILE_VALUE_MASK);
			}
			glColor3f (1.0, 1.0, 1.0);
			if (renderArgs.gameStatus[i][j] & TILE_FLAGGED)	/* If the tile was flagged (right-clicked) */
				glCallList (_dlFlag);
			if ((renderArgs.gameStatus[i][j] & TILE_BOMB) && (renderArgs.gameStatus[i][j] & TILE_CLEARED))	/* If we cleared */
				glCallList (_dlBomb);																		/* a mined tile */
			glPopMatrix();
		}
	}
	
	glDisable (GL_TEXTURE_2D);
	
	renderArgs._testScreen.render();
}

void RenderEngine::loadResources()
{
	loadTextures();
	buildDisplayLists();
	_textEngine.loadTextures();
}

void RenderEngine::buildDisplayLists()
{
	_dlFlag = glGenLists(1);
	_dlTile = glGenLists(1);
	_dlBomb = glGenLists(1);
	_dlButton = glGenLists(1);
	
	glNewList(_dlFlag, GL_COMPILE);
		glBindTexture(GL_TEXTURE_2D, _texFlag);
		glPushMatrix();
		glScaled(0.9, 0.9, 1.0);
		glTranslated(0.1, 0.1, 0.0);
		glBegin (GL_QUADS);
			glTexCoord2d(1.0, 0.0);	glVertex2d(1.0, 1.0);
			glTexCoord2d(1.0, 1.0);	glVertex2d(1.0, 0.0);
			glTexCoord2d(0.0, 1.0);	glVertex2d(0.0, 0.0);
			glTexCoord2d(0.0, 0.0);	glVertex2d(0.0, 1.0);
		glEnd();
		glPopMatrix();
	glEndList();
	
	glNewList(_dlTile, GL_COMPILE);
		glBindTexture(GL_TEXTURE_2D, _texTile);
		glBegin (GL_QUADS);
			glTexCoord2d(1.0, 0.0);	glVertex2d(1.0, 1.0);
			glTexCoord2d(1.0, 1.0);	glVertex2d(1.0, 0.0);
			glTexCoord2d(0.0, 1.0);	glVertex2d(0.0, 0.0);
			glTexCoord2d(0.0, 0.0);	glVertex2d(0.0, 1.0);
		glEnd();
	glEndList();
	
	glNewList(_dlBomb, GL_COMPILE);
		glBindTexture(GL_TEXTURE_2D, _texBomb);
		glBegin (GL_QUADS);
			glTexCoord2d(1.0, 0.0);	glVertex2d(1.0, 1.0);
			glTexCoord2d(1.0, 1.0);	glVertex2d(1.0, 0.0);
			glTexCoord2d(0.0, 1.0);	glVertex2d(0.0, 0.0);
			glTexCoord2d(0.0, 0.0);	glVertex2d(0.0, 1.0);
		glEnd();
	glEndList();
	
	glNewList(_dlButton, GL_COMPILE);
		glBindTexture(GL_TEXTURE_2D, _texButton);
		glBegin (GL_QUADS);
			glTexCoord2d(1.0, 0.0);	glVertex2d(0.5, 0.5);
			glTexCoord2d(1.0, 1.0);	glVertex2d(0.5, -0.5);
			glTexCoord2d(0.0, 1.0);	glVertex2d(-0.5, -0.5);
			glTexCoord2d(0.0, 0.0);	glVertex2d(-0.5, 0.5);
		glEnd();
	glEndList();
}

void RenderEngine::loadTextures ()
{
	_texFlag = loadTexture ("Flag.png");
	_texTile = loadTexture ("Tile_simple.png");
	_texBomb = loadTexture ("Bomb.png");
	_texButton = loadTexture ("Button.png");
}

/* I don't think I need to tell what this function does ? */
void RenderEngine::render ( RenderArgs &renderArgs )
{
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	/* Setting up for 2D HUD render */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity ();
	/* 1 Gl unit = 1 tile, easier :P */
	gluOrtho2D(0.0, renderArgs.nTilesWidth, renderArgs.nTilesHeight + 2, 0.0);
	// gluOrtho2D (0.0, 640.0, 0.0, 480.0);
	glDisable (GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	
	drawHUD ( renderArgs );
	
	glFlush();
	SDL_GL_SwapBuffers();
}

/* Pretty self-explicit */
void RenderEngine::reload ( RenderArgs &renderArgs )
{
	if (_wasLoaded)		SDL_Quit ();
	_wasLoaded = false;
	load ( renderArgs );
}

/* Will set up everything for the render context (SDL & OpenGL) */
bool RenderEngine::load ( RenderArgs &renderArgs )
{
	if (_wasLoaded)		return true;
	
	SDL_Surface *scr = NULL;
	SDL_Init ( SDL_INIT_VIDEO | SDL_INIT_TIMER );
	scr = SDL_SetVideoMode ( renderArgs.nTilesWidth * TILE_SIZE, (renderArgs.nTilesHeight + 2) * TILE_SIZE, 32, SDL_OPENGL );
	glEnable(GL_DEPTH_TEST);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity ();
	// gluPerspective (FOV,(double)WIN_W/WIN_H,0.5,1000);	// Edit here to change GL View caracteristics
	
	SDL_EnableUNICODE (1);
	
	glClearColor(0.75, 0.75, 0.75, 1.0);
	
	SDL_WM_SetCaption("Minesweeper", NULL);
	
	return (scr) ? true : false;
}








