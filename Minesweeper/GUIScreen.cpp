//
//  GUIScreen.cpp
//  Minesweeper
//
//  Created by Alexis on 16/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUIScreen.h"

GUIScreen::GUIScreen (double width, double height, double x, double y) : GUI(width, height, x, y)
{
	GLcolor bgColor = {0, 0, 0, 0.5};
	GLcolor olColor = {0};	/* No outline */
	
	_isShown = false;
	
	_nElements = 0;
	_bgTexture = 0;
	
	_bgColorBottomLeft = _bgColorBottomRight = _bgColorTopLeft = _bgColorTopRight = bgColor;
	_olColorBottomLeft = _olColorBottomRight = _olColorTopLeft = _olColorTopRight = olColor;
}

GUIScreen::~GUIScreen ()
{
	for (int i = 0; i < _nElements; i++)
		delete _guiElements.at (i);
}

void GUIScreen::addElement (GUIElement *guiElement)
{
	_guiElements.push_back (guiElement);
	_nElements++;
}

void GUIScreen::render()
{
	if (!_isShown)	return;
	
	/* Rendering the backgroud of the GUI */
	if (_bgTexture)	{
		glEnable (GL_TEXTURE_2D);
		glBindTexture (GL_TEXTURE_2D, _bgTexture);
	} else glDisable (GL_TEXTURE_2D);
	
	/* Background with its colors */
	glBegin (GL_QUADS);
		glColor4f (_bgColorTopLeft.r, _bgColorTopLeft.g, _bgColorTopLeft.b, _bgColorTopLeft.a);	glTexCoord2f (0.0, 0.0);	glVertex2d (_x, _y);
		glColor4f(_bgColorBottomLeft.r, _bgColorBottomLeft.g, _bgColorBottomLeft.b, _bgColorBottomLeft.a); glTexCoord2f (0.0, 1.0);		glVertex2d (_x, _y + _height);
		glColor4f (_bgColorBottomRight.r, _bgColorBottomRight.g, _bgColorBottomRight.b, _bgColorBottomRight.a); glTexCoord2f (1.0, 1.0);  glVertex2d (_x + _width, _y + _height);
		glColor4f (_bgColorTopRight.r, _bgColorTopRight.g, _bgColorTopRight.b, _bgColorTopRight.a); glTexCoord2f (1.0, 0.0); glVertex2d (_x + _width, _y);
	glEnd ();
	
	glDisable (GL_TEXTURE_2D);
	/* Outline */
	glBegin (GL_LINE_LOOP);
		glColor4f (_olColorTopLeft.r, _olColorTopLeft.g, _olColorTopLeft.b, _olColorTopLeft.a);	glVertex2d (_x, _y);
		glColor4f(_olColorBottomLeft.r, _olColorBottomLeft.g, _olColorBottomLeft.b, _olColorBottomLeft.a);	glVertex2d (_x, _y + _height);
		glColor4f (_olColorBottomRight.r, _olColorBottomRight.g, _olColorBottomRight.b, _olColorBottomRight.a);	glVertex2d (_x + _width, _y + _height);
		glColor4f (_olColorTopRight.r, _olColorTopRight.g, _olColorTopRight.b, _olColorTopRight.a);	glVertex2d (_x + _width, _y);
	glEnd ();
	
	/* Rendering each element of the GUI */
	glPushMatrix();
	glTranslatef (_x, _y, 0.0);
	for (int i = 0; i < _nElements; i++)
		_guiElements.at (i)->render();
	glPopMatrix();
}

void GUIScreen::processLeftClick (double x, double y, bool buttonUp)
{
	for (int i = 0; i < _nElements; i++)
		_guiElements.at (i)->processLeftClick (x - _x, y - _y, buttonUp);
}

void GUIScreen::processRightClick (double x, double y, bool buttonUp)
{
	for (int i = 0; i < _nElements; i++)
		_guiElements.at (i)->processRightClick (x - _x, y - _y, buttonUp);
}

void GUIScreen::mouseMotion (double x, double y, bool isClicking)
{
	for (int i = 0; i < _nElements; i++)
		_guiElements.at (i)->mouseMotion (x - _x, y - _y, isClicking);
}

void GUIScreen::processKeypress (SDL_KeyboardEvent keyboardEvent)
{
	for (int i = 0; i < _nElements; i++)
		_guiElements.at (i)->processKeypress (keyboardEvent);
}

