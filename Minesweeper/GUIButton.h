//
//  GUIButton.h
//  Minesweeper
//
//  Created by Alexis on 17/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_BUTTON_H
#define _GUI_BUTTON_H

#include <iostream>
#include <string>
#include "GUIElement.h"

class GUIButton : public GUIElement
{
public:
	GUIButton (float x, float y, std::string title = "Button");
	~GUIButton ();
	
	/* The only thing that matters with a button xD */
	bool isClicked ()	{ return _isClicked; }
	
	/* The string contained in a button */
	const std::string& text ()	{ return _text; }
	void setText (const std::string &string)	{ _text = string; }
	
	/* Event processing */
	void processLeftClick (double x, double y, bool buttonUp);	/* x & y are given in GL coordinates */
	void mouseMotion (double x, double y, bool isClicking);
	
	/* Rendering */
	void render ();
	
private:
	bool _isClicked;
	bool _isToggled;
	bool _clickBeganOnMe;
	
	/* The text contained by the button */
	std::string _text;
};

#endif /* defined(_GUI_BUTTON_H) */
