//
//  GUITextEdit.cpp
//  Minesweeper
//
//  Created by Alexis on 23/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUITextEdit.h"

GUITextEdit::GUITextEdit (double width, double height, double x, double y, std::string defaultText) : GUIElement (width, height, x, y),
				_text(defaultText), _clickBeganOnMe(false), _editing(false)
{
	_maxText = "";
	for (int i = 0; i < _width; i++)
		_maxText += ' ';
}

GUITextEdit::~GUITextEdit() {}

void GUITextEdit::processLeftClick (double x, double y, bool buttonUp)
{
	if (buttonUp)	{
		_editing = (x >= _x && x <= _x + _width && y >= _y && y <= _y + _height && _clickBeganOnMe);
		_clickBeganOnMe = false;
	} else {
		_clickBeganOnMe = (x >= _x && x <= _x + _width && y >= _y && y <= _y + _height);
		_editing = false;
	}
}

void GUITextEdit::processKeypress (SDL_KeyboardEvent keybordEvent)
{
	if (_editing)	{
		if (keybordEvent.keysym.unicode == 0 ||			/* This is non-usable char (alt, ctrl, ...) */
			keybordEvent.keysym.unicode > 255)	return; /* And Unicode characters, not supported at the moment */
		if (keybordEvent.keysym.sym == SDLK_ESCAPE || keybordEvent.keysym.sym == SDLK_RETURN) return;
		if (keybordEvent.keysym.sym == SDLK_BACKSPACE)
			_text.pop_back();	/* Removing the last character */
		else if (_textEngine.getStringWidth (_text) < _width)
			_text += (unsigned char)keybordEvent.keysym.unicode;
	}
}

void GUITextEdit::render ()
{
	glPushMatrix();
	glTranslated (_x, _y, 0.0);
	std::string renderString = _text;
	if (_editing)	renderString += '|';
	_textEngine.drawBackground (_maxText, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0);
	_textEngine.drawString (renderString);
	glPopMatrix();
}
