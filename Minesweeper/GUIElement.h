//
//  GUIElement.h
//  Minesweeper
//
//  Created by Alexis on 16/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_ELEMENT_H
#define _GUI_ELEMENT_H

#include <iostream>
#include "GUI.h"

class GUIElement : public GUI
{
public:
	GUIElement (double width, double height, double x = 0.0, double y = 0.0);
	~GUIElement ();
	
	/* This determines if this GUI element currently has the focus in the GUI screen */
	bool hasFocus () { return _hasFocus; }
	
	virtual void render () {}
	virtual void processLeftClick (double x, double y, bool buttonUp) {}
	virtual void processRightClick (double x, double y, bool buttonUp) {}
	virtual void mouseMotion (double x, double y, bool isClicking) {}
	virtual void processKeypress (SDL_KeyboardEvent keyboardEvent) {}
	
protected:
	bool _hasFocus;
};

#endif /* defined(_GUI_ELEMENT_H) */
