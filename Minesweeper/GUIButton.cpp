//
//  GUIButton.cpp
//  Minesweeper
//
//  Created by Alexis on 17/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUIButton.h"

GUIButton::GUIButton (float x, float y, std::string title) : GUIElement (_textEngine.getStringWidth(title) + 0.2, 1.2, x, y),
															_text(title)
{
	_isToggled = false;
	_isClicked = false;
	_clickBeganOnMe = false;
}

GUIButton::~GUIButton()
{
	
}

void GUIButton::processLeftClick (double x, double y, bool buttonUp)
{
	_isClicked = false;
	
	if (buttonUp)	/* if we're releasing the click */
	{
		if (x >= _x && x <= _x + _width && y >= _y && y <= _y + _height && _clickBeganOnMe)	{
			_isToggled = false;
			_isClicked = true;
		} else {
			_isToggled = false;
			_isClicked = false;
		}
		_clickBeganOnMe = false;
	} else		/* If we clicked */
	{
		if (x >= _x && x <= _x + _width && y >= _y && y <= _y + _height)	{
			_clickBeganOnMe = true;
			_isToggled = true;
		} else {
			_clickBeganOnMe = false;
			_isToggled = false;
		}
	}
}

void GUIButton::mouseMotion (double x, double y, bool isClicking)
{
	if (isClicking)
	{
		_isToggled = (x >= _x && x <= _x + _width && y >= _y && y <= _y + _height && _clickBeganOnMe);
	} else
		_clickBeganOnMe = false;
}

void GUIButton::render ()
{
	glPushMatrix();
	glTranslatef (_x, _y, 0.0);
	
	/* Background of the button */
	if (_isToggled)
		_textEngine.drawBackground (_text, 0.5, 0.5, 0.5, 0.4, 0.3, 0.3, 0.3, 0.8);
	else
		_textEngine.drawBackground (_text, 0.9, 0.9, 0.9, 0.8, 0.7, 0.7, 0.7, 0.8);
	
	/* Text rendering code will follow, but not now xD */
	glTranslatef (0.4, 0.0, 0.0);
	_textEngine.drawString (_text);
	glPopMatrix();
}






