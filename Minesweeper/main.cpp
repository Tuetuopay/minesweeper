/*
 *  main.cpp
 *  Minesweeper
 *
 *  Created by Alexis on 12/04/13.
 *  Copyright (c) 2013 Tuetuopay. All rights reserved.
 *
 */

#include <iostream>
#include <ctime>
#include <SDL/SDL.h>

#include "sdlglutils.h"

#include "GameEngine.h"

#define WIN_W	640
#define WIN_H	480
#define FOV		90

int main (int argc, char ** argv)
{
	srand ((unsigned int)time(NULL));
	
	GameEngine gameEngine;
	
	gameEngine.run ();
	
	SDL_Quit ();
	
	return 0;
}