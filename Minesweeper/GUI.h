//
//  GUI.h
//  Minesweeper
//
//  Created by Alexis on 16/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_H
#define _GUI_H

#include <iostream>
#include <string>
#include "sdlglutils.h"

#include "TextEngine.h"

typedef struct GUIAction GUIAction;

class GUI	{
public:
	/* Width, height, x and y are given in GL units */
	GUI (double width, double height, double x = 0.0, double y = 0.0);
	~GUI ();
	
	/* Will redefine the gui's geometry */
	void resize (double width, double height);
	void move   (double x, double y);
	
	/* Renders the GUI. Needs to be overriden by GUIs */
	void render ();
	
	/* Event processing. Implemented by child classes
	 bool buttonUp stands for the event is BUTTONDOWN or BUTTONUP */
	void processLeftClick (double x, double y, bool buttonUp);	/* x & y are given in GL coordinates */
	void processRightClick (double x, double y, bool buttonUp);
	void mouseMotion (double x, double y, bool isClicking);
	void processKeypress (SDL_KeyboardEvent keyboardEvent);
	
protected:
	/* GUI's geometry */
	double _width, _height, _x, _y;
	
	/* So that all GUIs will have a text engine without the need to redeclare one */
	TextEngine	_textEngine;
};

struct GUIAction
{
	/* If this is a button leading to another GUI screen */
	bool isLeadingToGuiScreen;
	GUI *targetGui;

	/* If this is a text field */
	std::string text;

	/* If this is an element returning a value */
	int intValue;
	float floatValue;
};

#endif /* defined(_GUI_H) */









