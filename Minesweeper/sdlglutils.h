#ifndef SDLGLUTILS_H
#define SDLGLUTILS_H

#include <SDL/SDL.h>
	// #include <Windows.h>
	// #include <GL/gl.h>
#include <GLUT/GLUT.h>

#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE 0x812F
#endif

typedef struct GLcolor GLcolor;
struct GLcolor {
	float r, g, b, a;
};

GLuint loadTexture(const char * filename,bool useMipMap = true);
int takeScreenshot(const char * filename);
void drawAxis(double scale = 1);
int initFullScreen(unsigned int * width = NULL,unsigned int * height = NULL);
int XPMFromImage(const char * imagefile, const char * XPMfile);
SDL_Cursor * cursorFromXPM(const char * xpm[]);

void drawStr(const char *str);

#endif //SDLGLUTILS_H
