//
//  GUI.cpp
//  Minesweeper
//
//  Created by Alexis on 16/05/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUI.h"

GUI::GUI (double width, double height, double x, double y) : _width(width), _height(height), _x(x), _y(y), _textEngine ()
{}

GUI::~GUI ()
{}

void GUI::resize (double width, double height)
{
	_width = width;		_height = height;
}

void GUI::move (double x, double y)
{
	_x = x;				_y = y;
}

void GUI::render()
{
	printf ("GUI renderer\n");
}

void GUI::mouseMotion (double x, double y, bool isClicking)
{
	
}

void GUI::processRightClick (double x, double y, bool buttonUp)
{
	
}

void GUI::processLeftClick (double x, double y, bool buttonUp)
{
	
}

void GUI::processKeypress (SDL_KeyboardEvent keyboardEvent)
{
	
}